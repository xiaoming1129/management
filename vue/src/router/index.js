import Vue from 'vue'
import VueRouter from 'vue-router'
import Manager from '../views/Manager.vue'
import User from '../views/User.vue'
import Home from '../views/Home.vue'
import Test from '../views/Test.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import ManagerInfo from '../components/ManagerInfo.vue'
import Files from "@/views/Files.vue";
import Role from "@/views/Role.vue";
import Menu from "@/views/Menu";
Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'Manager',
    component: Manager,
    children:[
      { path:'/user', name:'User', component: User,meta:{title:'系统管理 / 用户管理'} },
      { path:'/home', name:'Home', component: Home,meta:{title:'主页'} },
      { path:'/test', name:'Test', component: Test,meta:{title:'测试'} },
      { path:'/managerInfo', name:'ManagerInfo', component: ManagerInfo,meta:{title:'个人信息'} },
      { path:'/files', name:'Files', component: Files,meta:{title:'系统管理 / 文件管理'} },
      { path:'/role', name:'Role', component: Role,meta:{title:'系统管理 / 角色管理'} },
      { path:'/menu', name:'Menu', component: Menu,meta:{title:'系统管理 / 菜单管理'} },
    ]
  },
  {
    path:'/',
    name:'Login',
    component: Login,
  },
  {
    path:'/register',
    name:'Register',
    component: Register,
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
