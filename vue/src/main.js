import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/global.css'
import JsonExcel from 'vue-json-excel'
Vue.use(ElementUI,{size:"small"});
Vue.config.productionTip = false
import echarts from 'echarts'
Vue.prototype.$echarts = echarts
Vue.component('downloadExcel', JsonExcel)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
