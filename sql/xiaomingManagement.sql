/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.19 : Database - leo_management
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`leo_management` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `leo_management`;

/*Table structure for table `article` */

DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '标题',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '内容',
  `user` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发布人',
  `time` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `article` */

insert  into `article`(`id`,`name`,`content`,`user`,`time`) values (1,'青哥哥的文章标题','# 我是青哥哥\n## 我是青哥哥2号\n\n::: hljs-center\n\n***++~~==我是那个哥哥==~~++***\n\n:::\n\n> 我是青哥哥的引用\n\n我是B站：程序员青戈\n\n[百度](https://www.baidu.com)\n\n```java\nclass Hello {\n  public static void main(String[] args) {\n    System.out.pringln(\"Hello 青哥哥\");\n  }\n}\n\n```\n\n![搜狗截图20220129174103.png](http://localhost:9090/file/8567a00d2bf740e0a63794baf600cec3.png)\n\n\n','程序员青戈','2022-03-22 19:22:58'),(2,'青哥哥文章2号','青哥哥文章2号\n\n青哥哥文章2号\n\n青哥哥文章2号\n\n![QQ图片20220307194920.png](http://localhost:9090/file/5e40a867acd74d1f90b0ac9a765823e5.png)','程序员青戈','2022-03-22 19:22:58');

/*Table structure for table `course` */

DROP TABLE IF EXISTS `course`;

CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '课程名称',
  `score` int(11) DEFAULT NULL COMMENT '学分',
  `times` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上课时间',
  `state` tinyint(1) DEFAULT NULL COMMENT '是否开课',
  `teacher_id` int(11) DEFAULT NULL COMMENT '授课老师id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `course` */

insert  into `course`(`id`,`name`,`score`,`times`,`state`,`teacher_id`) values (1,'大学物理',10,'40',0,17),(2,'高等数学',10,'45',NULL,16),(3,'大学英语',10,'30',NULL,16);

/*Table structure for table `student_course` */

DROP TABLE IF EXISTS `student_course`;

CREATE TABLE `student_course` (
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`student_id`,`course_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `student_course` */

insert  into `student_course`(`student_id`,`course_id`) values (28,1),(28,2);

/*Table structure for table `sys_dict` */

DROP TABLE IF EXISTS `sys_dict`;

CREATE TABLE `sys_dict` (
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '内容',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类型'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `sys_dict` */

insert  into `sys_dict`(`name`,`value`,`type`) values ('user','el-icon-user','icon'),('house','el-icon-house','icon'),('menu','el-icon-menu','icon'),('s-custom','el-icon-s-custom','icon'),('s-grid','el-icon-s-grid','icon'),('document','el-icon-document','icon'),('coffee','el-icon-coffee\r\n','icon'),('s-marketing','el-icon-s-marketing','icon');

/*Table structure for table `sys_file` */

DROP TABLE IF EXISTS `sys_file`;

CREATE TABLE `sys_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件名称',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件类型',
  `size` bigint(20) DEFAULT NULL COMMENT '文件大小(kb)',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '下载链接',
  `md5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文件md5',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `enable` tinyint(1) DEFAULT '1' COMMENT '是否禁用链接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `sys_file` */

insert  into `sys_file`(`id`,`name`,`type`,`size`,`url`,`md5`,`is_delete`,`enable`) values (17,'649002da71c8473db39892b4a758f875.png','png',634,'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png','e1a9407cd0e937c4b411a93b7aca7c87',1,0),(18,'1641024983532cf.png','png',634,'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png','e1a9407cd0e937c4b411a93b7aca7c87',1,0),(19,'Java3年经验开发个人简历模板.doc','doc',47,'http://localhost:9090/file/0e020e1337474a93ba3b43a75b2890ee.doc','9ace4fac24420f85c753aa663009edb4',1,1),(20,'1626102561055-2.jpg','jpg',24,'http://localhost:9090/file/cd379f381364482aaad0d6ffb7209d3d.jpg','adb0481b283645af3809e3d8a1bdb294',1,1),(21,'1622011842830-5.jpg','jpg',14,'http://localhost:9090/file/7737484487db47ab89e58504ddf86ac1.jpg','2dcd5d60c696c47fdfe0b482c18de0ea',0,1),(22,'1622536738094-7.jpg','jpg',12,'http://localhost:9090/file/ad5946fe27c14508ac796115f6465826.jpg','35977e7dc2159a734f3c81de460e4b8d',0,1),(23,'用户信息 (1).xlsx','xlsx',3,'http://localhost:9090/file/02f70e07e69045c38b4748283ffeeabb.xlsx','687f150737c967e74cfd6fa9ec981589',0,1),(24,'1641024983532cf.png','png',634,'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png','e1a9407cd0e937c4b411a93b7aca7c87',0,1),(25,'1641024983532cf.png','png',634,'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png','e1a9407cd0e937c4b411a93b7aca7c87',0,1),(26,'6.jpg','jpg',30,'http://localhost:9090/file/9b21a2b133b140e0bcd9bf66dc5cad1d.jpg','a2cf10bcbed5e9d98cbaf5467acae28d',0,1),(28,'1622011842830-5.jpg','jpg',14,'http://localhost:9090/file/7737484487db47ab89e58504ddf86ac1.jpg','2dcd5d60c696c47fdfe0b482c18de0ea',1,1),(29,'1641024983532cf (1).png','png',634,'http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png','e1a9407cd0e937c4b411a93b7aca7c87',0,1),(30,'1622011842830-5.jpg','jpg',14,'http://localhost:9090/file/7737484487db47ab89e58504ddf86ac1.jpg','2dcd5d60c696c47fdfe0b482c18de0ea',0,1),(31,'QQ截图20211214232106.jpg','jpg',30,'http://localhost:9090/file/7de0e50f915547539db12023cf997276.jpg','ba5dd263551a31d9bb0582b07cb480e1',0,1),(32,'boot.jpg','jpg',11,'http://localhost:9090/file/657d7054d7864bd7a0aaba9e44f7924e.jpg','2ab82ad78ff081665ee90f8cb38b45db',0,1),(33,'QQ图片20210828212629.gif','gif',188,'http://localhost:9090/file/e5512c68a5614135a12064afa66c67e5.gif','ce524058758a83c046b97c66ddcb8a83',0,1),(34,'vite.jpg','jpg',27,'http://localhost:9090/file/782f20b37b8b4a158c5e13a07fe826d5.jpg','c67bab9c32968cd0cda3e1608286b0d9',0,1),(35,'mp.jpg','jpg',32,'http://localhost:9090/file/650e8330e78b4fed8fb0c1d866684b5d.jpg','cb887a9d64563352edce80cf50296cc5',0,1),(36,'qq音乐.png','png',445,'http://localhost:9090/file/461504596ec040729776b674ddec88d3.png','793fd534fa705475eb3358f68c87ec68',0,1),(37,'QQ截图20211214232106.jpg','jpg',30,'http://localhost:9090/file/7de0e50f915547539db12023cf997276.jpg','ba5dd263551a31d9bb0582b07cb480e1',0,1),(38,'boot.jpg','jpg',11,'http://localhost:9090/file/657d7054d7864bd7a0aaba9e44f7924e.jpg','2ab82ad78ff081665ee90f8cb38b45db',0,1),(39,'QQ截图20211214232106.jpg','jpg',30,'http://localhost:9090/file/7de0e50f915547539db12023cf997276.jpg','ba5dd263551a31d9bb0582b07cb480e1',0,0),(40,'v1.mp4','mp4',47484,'http://localhost:9090/file/a22c9d62ef0648db86b9766bb14d742a.mp4','3dda54bc1a07bd9112bfb381c20b4867',0,1),(41,'搜狗截图20220129174103.png','png',56,'http://localhost:9090/file/8567a00d2bf740e0a63794baf600cec3.png','050df6119399582fda666834870608d7',0,0),(42,'QQ图片20220307194920.png','png',100,'http://localhost:9090/file/5e40a867acd74d1f90b0ac9a765823e5.png','0f1337b5c0ebf68f67718fcf42d1322f',0,0),(43,'QQ图片20220307194920.png','png',100,'http://localhost:9090/file/5e40a867acd74d1f90b0ac9a765823e5.png','0f1337b5c0ebf68f67718fcf42d1322f',0,1),(57,'曾星铭.pdf','pdf',267,'http://localhost:8080/file/download/ae572944970940eabcc5f0c12d551da8.pdf','5e1c8f9ca599282b85aa10e98400fdb5',0,0),(66,'92a1d07ddad34aa4b77edf6dd7c9812c!400x400.jpeg','jpeg',27,'http://localhost:8080/file/download/7bb0378759684969beaaedb27d4d5989.jpeg','91b2355318dfbdf49a6b4dff22ba8c6f',0,1),(68,'92a1d07ddad34aa4b77edf6dd7c9812c!400x400.jpeg','jpeg',27,'http://localhost:8080/file/download/7bb0378759684969beaaedb27d4d5989.jpeg','91b2355318dfbdf49a6b4dff22ba8c6f',0,1),(69,'5730ba7462c6615e!400x400.jpg','jpg',17,'http://localhost:8080/file/download/a5f50faea07947ed909840e828490558.jpg','35585508f22ff378179a0d065397ffa2',0,1),(70,'92a1d07ddad34aa4b77edf6dd7c9812c!400x400.jpeg','jpeg',27,'http://localhost:8080/file/download/7bb0378759684969beaaedb27d4d5989.jpeg','91b2355318dfbdf49a6b4dff22ba8c6f',0,0),(71,'5730ba7462c6615e!400x400.jpg','jpg',17,'http://localhost:8080/file/download/a5f50faea07947ed909840e828490558.jpg','35585508f22ff378179a0d065397ffa2',0,1),(72,'f959a4d72f0d435d9cff866ee4e52565!400x400.jpeg','jpeg',22,'http://localhost:8080/file/download/ff640d9c91404de2bfef19bce5e9acb2.jpeg','99e79cbfb9bddc88d42ecb0f56c22b7e',0,0),(73,'f959a4d72f0d435d9cff866ee4e52565!400x400.jpeg','jpeg',22,'http://localhost:8080/file/download/ff640d9c91404de2bfef19bce5e9acb2.jpeg','99e79cbfb9bddc88d42ecb0f56c22b7e',1,1);

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '路径',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图标',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '描述',
  `pid` int(11) DEFAULT NULL COMMENT '父级id',
  `page_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '页面路径',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`id`,`name`,`path`,`icon`,`description`,`pid`,`page_path`,`sort_num`) values (2,'数据报表','/dashbord','el-icon-s-marketing','11',NULL,'Dashbord',100),(4,'系统管理',NULL,'el-icon-menu',NULL,NULL,'Manager',100),(5,'用户管理','/user','el-icon-user',NULL,4,'User',301),(6,'角色管理','/role','el-icon-s-custom',NULL,4,'Role',302),(7,'菜单管理','/menu','el-icon-menu',NULL,4,'Menu',303),(8,'文件管理','/file','el-icon-document',NULL,4,'File',304),(9,'请作者喝杯咖啡','/donate','el-icon-coffee\r\n',NULL,NULL,'Donate',200),(10,'主页','/home','el-icon-house',NULL,NULL,'Home',0),(39,'课程管理','/course','el-icon-menu',NULL,NULL,'Course',201),(40,'高德地图','/map','el-icon-house',NULL,NULL,'Map',999),(41,'文章管理','/article','el-icon-menu','阿的说法',NULL,'Article',999);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '描述',
  `flag` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '唯一标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`name`,`description`,`flag`) values (1,'管理员','管理员','ROLE_ADMIN'),(2,'学生','学生','ROLE_STUDENT'),(3,'老师','老师','ROLE_TEACHER');

/*Table structure for table `sys_role_menu` */

DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `menu_id` int(11) NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='角色菜单关系表';

/*Data for the table `sys_role_menu` */

insert  into `sys_role_menu`(`role_id`,`menu_id`) values (1,2),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,39),(1,40),(1,41),(3,2),(3,10),(3,39);

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '昵称',
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '电话',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `avatar_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '头像',
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '角色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`username`,`password`,`nickname`,`email`,`phone`,`address`,`create_time`,`avatar_url`,`role`) values (1,'admin','admin','小扬子','admin@qq.com','13988997788','安徽合肥','2022-01-22 21:10:27','http://localhost:8080/file/download/a5f50faea07947ed909840e828490558.jpg','ROLE_ADMIN'),(2,'杨丽华','123456','洋洋','12345785@qq.com','15789653254','山东济南','2022-01-20 13:31:12',NULL,NULL),(3,'曾星铭','123456','小铭同学','527495509@qq.com','15022224451','山西运城','2022-04-20 10:51:53',NULL,NULL),(4,'胡城皓','123456','胡逼','ad2113653@163.com','13756954123','河北唐山','2022-02-20 11:50:30',NULL,NULL),(5,'曾晨宇','123456','多多','1234584@139.com','12544956325','山西运城','2022-01-21 09:49:54',NULL,NULL),(6,'张杰','123456','杰哥','9556532@163.com','13245789765','山东德州','2022-06-21 09:49:54',NULL,NULL),(7,'张霖','124566','歌唱家','5996562@qq.com','15789754234','辽宁大连','2022-07-21 09:52:02',NULL,NULL),(8,'胡图图','123456','大耳朵','46548975@139.com','13247984564','北京','2022-08-21 09:52:02',NULL,NULL),(23,'雷猴','123456','猴子','1321@163.ccom','35486451321','北京','2022-09-24 17:30:58','http://localhost:9090/file/8d966b0e6cf84fe191a72a58b8293b23.png','ROLE_ADMIN');

/*Table structure for table `t_comment` */

DROP TABLE IF EXISTS `t_comment`;

CREATE TABLE `t_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '内容',
  `user_id` int(11) DEFAULT NULL COMMENT '评论人id',
  `time` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '评论时间',
  `pid` int(11) DEFAULT NULL COMMENT '父id',
  `origin_id` int(11) DEFAULT NULL COMMENT '最上级评论id',
  `article_id` int(11) DEFAULT NULL COMMENT '关联文章的id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `t_comment` */

insert  into `t_comment`(`id`,`content`,`user_id`,`time`,`pid`,`origin_id`,`article_id`) values (1,'测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试',1,'2022-03-22 20:00:00',NULL,NULL,1),(2,'123',NULL,NULL,NULL,NULL,NULL),(5,'回复内容',1,'2022-03-22 21:01:00',NULL,NULL,1),(6,'4444',1,'2022-03-22 21:03:15',4,4,1),(7,'5555',1,'2022-03-22 21:04:11',4,4,1),(8,'444444',1,'2022-03-22 21:29:55',7,7,1),(9,'5555',1,'2022-03-22 21:30:04',7,7,1),(10,'666',1,'2022-03-22 21:34:05',7,4,1),(11,'甄姬真的好大好大！！',16,'2022-03-22 21:38:26',10,4,1),(13,'哈哈哈哈，我是ddd',28,'2022-03-22 21:46:01',12,12,1),(14,'我是李信，我很萌',20,'2022-03-22 21:46:48',13,12,1),(15,'我在回复ddd',20,'2022-03-22 21:47:03',13,12,1),(16,'我是李信',20,'2022-03-22 21:48:19',4,4,1),(17,'33333',20,'2022-03-22 21:48:42',5,5,1),(19,'我是李信嗯嗯嗯',20,'2022-03-22 21:49:21',1,1,1),(21,'哈哈哈 我是ddd',28,'2022-03-22 21:50:04',20,1,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
