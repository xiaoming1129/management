package com.leo.springboot.mapper;

import com.leo.springboot.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author leo
 * @since 2022-06-22
 */
public interface UserMapper extends BaseMapper<User> {

}
