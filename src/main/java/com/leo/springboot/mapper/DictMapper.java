package com.leo.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.leo.springboot.entity.Dict;

public interface DictMapper extends BaseMapper<Dict> {
}
