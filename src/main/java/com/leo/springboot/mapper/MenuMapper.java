package com.leo.springboot.mapper;

import com.leo.springboot.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author leo
 * @since 2022-07-05
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
