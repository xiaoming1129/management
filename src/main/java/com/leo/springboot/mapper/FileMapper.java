package com.leo.springboot.mapper;

import com.leo.springboot.entity.Files;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author leo
 * @since 2022-07-01
 */
public interface FileMapper extends BaseMapper<Files> {

}
