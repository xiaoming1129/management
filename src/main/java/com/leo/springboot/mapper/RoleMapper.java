package com.leo.springboot.mapper;

import com.leo.springboot.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author leo
 * @since 2022-07-05
 */
public interface RoleMapper extends BaseMapper<Role> {

    @Select("select id from leo_management.sys_role where flag=#{flag}")
    Integer selectByFlag(@Param("flag") String flag);
}
