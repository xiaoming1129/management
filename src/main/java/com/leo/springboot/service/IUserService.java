package com.leo.springboot.service;

import com.leo.springboot.controller.dto.UserDTO;
import com.leo.springboot.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author leo
 * @since 2022-06-22
 */
public interface IUserService extends IService<User> {

    UserDTO login(UserDTO userDTO);

    UserDTO register(UserDTO userDTO);
}
