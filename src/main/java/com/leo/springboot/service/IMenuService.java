package com.leo.springboot.service;

import com.leo.springboot.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author leo
 * @since 2022-07-05
 */
public interface IMenuService extends IService<Menu> {

    List<Menu> findAll(String name);

}
