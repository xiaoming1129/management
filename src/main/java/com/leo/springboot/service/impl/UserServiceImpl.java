package com.leo.springboot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.log.Log;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.leo.springboot.common.Constants;
import com.leo.springboot.common.Result;
import com.leo.springboot.controller.dto.UserDTO;
import com.leo.springboot.entity.Menu;
import com.leo.springboot.entity.User;
import com.leo.springboot.exception.ServiceException;
import com.leo.springboot.mapper.RoleMapper;
import com.leo.springboot.mapper.RoleMenuMapper;
import com.leo.springboot.mapper.UserMapper;
import com.leo.springboot.service.IMenuService;
import com.leo.springboot.service.IRoleService;
import com.leo.springboot.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.leo.springboot.util.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author leo
 * @since 2022-06-22
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    private static final Log LOG = Log.get();

    @Resource
    private RoleMapper roleMapper;
    @Resource
    private RoleMenuMapper roleMenuMapper;
    @Resource
    private IMenuService menuService;
    @Override
    public UserDTO login(UserDTO userDTO) {

        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.eq("username", userDTO.getUsername());
        queryWrapper.eq("password", userDTO.getPassword());
        User user;
        try {
            user = getOne(queryWrapper);
        }catch (Exception e){
            LOG.error(e);
            throw new ServiceException(Constants.CODE_500, "系统错误！");
        }
        if (user != null) {
            BeanUtil.copyProperties(user, userDTO, true);
            String token = TokenUtils.genToken(user.getId().toString(), user.getPassword());
            userDTO.setToken(token);
            String role = userDTO.getRole();
            Integer roleId = roleMapper.selectByFlag(role);
            List<Menu> roleMenus = getRoleMenus(roleId);
            userDTO.setMenus(roleMenus);
            return userDTO;
        } else {
            throw new ServiceException(Constants.CODE_600, "用户名或密码错误!");
        }

    }

    @Override
    public UserDTO register(UserDTO userDTO) {
        String username = userDTO.getUsername();
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.eq("username",username);
        User user = getOne(queryWrapper);
        if(user!=null){
            throw new ServiceException(Constants.CODE_600,"用户名已存在，请重新输入！");
        }else{
            user = new User();
            BeanUtil.copyProperties(userDTO,user,true);
            save(user);
            return userDTO;
        }
    }
    private List<Menu> getRoleMenus(Integer roleId){

        List<Integer> menuIds = roleMenuMapper.selectByRoleId(roleId);
        List<Menu> roleMenus = new ArrayList<>();
        //查出系统所有的菜单
        List<Menu> menus = menuService.findAll("");
        for (Menu menu : menus) {
            if(menuIds.contains(menu.getId())){
                roleMenus.add(menu);
            }
            List<Menu> children = menu.getChildren();


            children.removeIf(child -> !menuIds.contains(child.getId()));
        }
        return roleMenus;
    }

}
