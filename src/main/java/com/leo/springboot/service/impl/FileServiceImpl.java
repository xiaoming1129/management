package com.leo.springboot.service.impl;

import com.leo.springboot.entity.Files;
import com.leo.springboot.mapper.FileMapper;
import com.leo.springboot.service.IFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author leo
 * @since 2022-07-01
 */
@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, Files> implements IFileService {

}
