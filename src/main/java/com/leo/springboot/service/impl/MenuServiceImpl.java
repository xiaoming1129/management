package com.leo.springboot.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.leo.springboot.entity.Menu;
import com.leo.springboot.mapper.MenuMapper;
import com.leo.springboot.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author leo
 * @since 2022-07-05
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {


    @Override
    public List<Menu> findAll(String name) {

        QueryWrapper<Menu> wrapper = new QueryWrapper<>();
        if(StrUtil.isNotBlank(name)){
            wrapper.like("name",name);
        }
        //查询所有数据
        List<Menu> list = list(wrapper);
        //通过stream流找到所有pid为null的一级菜单
        List<Menu> parentMenu = list.stream().filter(menu -> menu.getPid() == null).collect(Collectors.toList());
        for (Menu menu : parentMenu) {
            //找到所有菜单对应的子菜单
            menu.setChildren(list.stream().filter(m -> menu.getId().equals(m.getPid())).collect(Collectors.toList()));
        }
        return parentMenu;
    }
}
