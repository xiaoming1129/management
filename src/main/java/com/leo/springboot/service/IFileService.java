package com.leo.springboot.service;

import com.leo.springboot.entity.Files;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author leo
 * @since 2022-07-01
 */
public interface IFileService extends IService<Files> {

}
